#!/bin/sh
set -e

echo "Starting entrypoint script..."

if [ "$1" = '--help' ]; then
    echo "HELP FILE: This is not meant to be very helpful. Lorem Ipsum ;-)"
	exit 1
fi

echo "Homework server is now running. Access via localhost:8080"

exec "$@"
