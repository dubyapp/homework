#FROM node:current-slim

FROM ubuntu:latest

MAINTAINER Russell Thomas | mail@russellthomas.com

WORKDIR /usr/src/app

COPY ./ /usr/src/app/

RUN chmod 777 /usr/src/app/entry.sh

#Update Build
RUN apt-get update && apt-get install -y nano && apt-get clean

#Install MySQL
RUN apt-get install -y mysql-server

#Update and Install Nano and Networking Tools
RUN apt-get install -y net-tools && apt-get install -y iputils-ping

EXPOSE 8080

ENTRYPOINT ["./entry.sh"]

CMD ["./server_linux_386"]