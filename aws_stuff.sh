#!/bin/sh

#create role
aws iam create-role --role-name homeworkRole --assume-role-policy-document aws_trust_policy.json

#attach
aws iam attach-role-policy --role-name homeworkRole --policy-arn arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess

#create and add
aws iam create-instance-profile --instance-profile-name homework_instance_profile
aws iam add-role-to-instance-profile --role-name homeworkRole --instance-profile-name homework_instance_profile

#assign policy to s3 bucket
aws s3api put-bucket-policy --bucket ibt-homework-bucket --policy aws_s3_bucket_policy.json