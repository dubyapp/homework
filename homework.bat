REM Homework App Installation Script 
REM Russell Thomas mail@russellthomas.com
REM 3/5/2020

docker image build -t words:v1 .
docker network create myNetwork
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_USER=admin -e MYSQL_PASSWORD=1234 -e MYSQL_DATABASE=homework --network myNetwork -d mysql/mysql-server:5.7
docker container run --publish 8080:8080 --network myNetwork --name homework words:v1

echo "Homework server is now running... Access via localhost:8080"

ping 127.0.0.1 -n 3

start "" http://localhost:8080/word

exit