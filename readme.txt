iBotta Interview Homework Instructions
Russell Thomas
March 5th, 2020

Dependencies:  Docker for Windows or Linux


---- From clean Linux install perform the following:

git clone git@bitbucket.org:dubyapp/homework.git

cd homework

open readme.txt

------------------------------------------------------------------------------

chmod +x homework.sh

./homework.sh (alternatively you can add --help, i.e. ./homework.sh --help)

This batch file creates two docker containers and a private network. 

When finished, pop anew console and access the HOMEWORK server locally using 

	curl http://localhost:8080/word

To test connecting to the MySQL Server form the homework server:

docker exec -it homework bash
mysql -h mysql -u admin -p
1234


----- From Windows PowerShell perform the following:

cd to location of files...

start-process homework.bat

This batch file creates two docker containers and a private network. 

When finished, you may access the HOMEWORK server locally using 

	http://localhost:8080/word

The MySQL Server is running in it's own container accessible over the private network as:

	mysql:3306

To test connecting to the MySQL Server form the homework server:

docker exec -it homework bash
mysql -h mysql -u admin -p
1234


AMAZON AWS STUFF

Assuming you are on a linux machine tied to an AWS account, run ./aws_stuff.sh

For now, just read the file and the associated JSON files

THANKS!

 - Russell Thomas
 - 303-503-7892
 - mail@russellthomas.com