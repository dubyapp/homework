#!/bin/sh
set -e

chmod +x server_linux_386
chmod +x entry.sh
chmod +x aws_stuff.sh

#create
docker image build -t words:v1 .
docker network create myNetwork
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_USER=admin -e MYSQL_PASSWORD=1234 -e MYSQL_DATABASE=homework --network myNetwork -d mysql/mysql-server:5.7
echo "MySQL server is now running... Access via mysql:3306 -u admin -p 1234"


if [ "$1" = '--help' ]; then
    docker container run --publish 8080:8080 --network myNetwork --name homework words:v1 --help
	exit 1
fi

docker container run --publish 8080:8080 --network myNetwork --name homework words:v1
echo "Homework server is now running... Access via localhost:8080"
